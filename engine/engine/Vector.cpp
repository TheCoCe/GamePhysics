#include "Vector.h"
namespace engine {

	Vector::Vector()
	{
		this->x = 0;
		this->y = 0;
		this->z = 0;
	}

	Vector::Vector(float x, float y, float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	Vector::~Vector()
	{
	}


	float Vector::X()
	{
		return this->x;
	}

	float Vector::Y()
	{
		return this->y;
	}

	float Vector::Z()
	{
		return this->z;
	}

	void Vector::X(float x)
	{
		this->x = x;
	}

	void Vector::Y(float y)
	{
		this->y = y;
	}

	void Vector::Z(float z)
	{
		this->z = z;
	}

	Vector& Vector::operator+=(const Vector& v)
	{
		this->x += v.x;
		this->y += v.y;
		this->z += v.z;

		return *this;
	}

	Vector& Vector::operator-=(const Vector& v)
	{
		this->x -= v.x;
		this->y -= v.y;
		this->z -= v.z;

		return *this;
	}
}