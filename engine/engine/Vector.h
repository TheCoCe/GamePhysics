#pragma once
namespace engine {
	class Vector
	{
	public:
		Vector();
		Vector(float x, float y, float z);
		~Vector();
		float X();
		float Y();
		float Z();
		void X(float x);
		void Y(float y);
		void Z(float z);
		Vector& operator+=(const Vector& v);
		Vector& operator-=(const Vector& v);

	private:
		float x, y, z;
	};
}

