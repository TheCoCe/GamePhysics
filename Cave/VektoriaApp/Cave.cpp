#include "Cave.h"

Cave::Cave(CHVector size)
{
	Cave(size.x, size.y, size.z, size.w);
}

Cave::Cave(float x, float y, float z, float w)
{
	x /= 2.f;
	y /= 2.f;
	z /= 2.f;
	for (int i = 0; i < 5; i++) {
		m_zpCave[i].AddGeo(&m_gcCave[i]);
	}

	//Boden
	m_gcCave[0].Init(CHVector(x, w, z), NULL);
	m_zpCave[0].Translate(CHVector(x, -w, -z));
	this->AddPlacement(&m_zpCave[0]);
	//Decke
	m_gcCave[1].Init(CHVector(x, w, z), NULL);
	m_zpCave[1].Translate(CHVector(x, (2.f * y) + w, -z));
	this->AddPlacement(&m_zpCave[1]);
	//Links
	m_gcCave[2].Init(CHVector(w, y, z), NULL);
	m_zpCave[2].Translate(CHVector(-w, y, -z));
	this->AddPlacement(&m_zpCave[2]);
	//Rechts
	m_gcCave[3].Init(CHVector(w, y, z), NULL);
	m_zpCave[3].Translate(CHVector((2.f * x) + w, y, -z));
	this->AddPlacement(&m_zpCave[3]);
	//Hinten
	m_gcCave[4].Init(CHVector(x, y, w), NULL);
	m_zpCave[4].Translate(CHVector(x, y, (2.f * -z) + w));
	this->AddPlacement(&m_zpCave[4]);
}

Cave::~Cave()
{
}
