#include "StdAfx.h"
#include "Game.h"



CGame::CGame(void)
{
}

CGame::~CGame(void)
{
}

void CGame::Init(HWND hwnd, void(*procOS)(HWND hwnd, unsigned int uWndFlags), CSplash * psplash)
{
	// Hier die Initialisierung Deiner Vektoria-Objekte einfügen:
	m_zr.Init(psplash);
	m_zc.Init(QUARTERPI);
	m_zf.Init(hwnd, procOS); 
	m_zv.InitFull(&m_zc);
	m_zl.Init(CHVector(1.0f, 1.0f, 1.0f), CColor(1.0f, 1.0f, 1.0f));

	m_zr.AddFrameHere(&m_zf);
	m_zf.AddViewport(&m_zv);
	m_zr.AddScene(&m_zs);
	m_zs.AddPlacement(&m_zpCamera);
	m_zs.AddLightParallel(&m_zl);
	m_zpCamera.AddCamera(&m_zc);
	m_zf.AddDeviceKeyboard(&m_dk);
	m_zf.AddDeviceMouse(&m_dm);

	cave = new Cave(caveSize, caveSize, caveSize, 0.1f);
	m_zs.AddPlacement(cave);

	m_zpS[0].AddGeo(&m_cgS[0]);
	m_zpS[1].AddGeo(&m_cgS[1]);
	m_cgS[0].Init(0.25f, NULL);
	m_cgS[1].Init(0.5f, NULL);
	m_zs.AddPlacement(&m_zpS[0]);
	m_zs.AddPlacement(&m_zpS[1]);
	m_zpS[0].Translate(3.f, 2.5f, -2.5f);
	m_zpS[1].Translate(1.f, 2.5f, -2.5f);

	m_zpCamera.Translate(caveSize / 2.f, caveSize / 2.f, 16.f);
}

void CGame::Tick(float fTime, float fTimeDelta)
{
	// Hier die Echtzeit-Veränderungen einfügen:
	m_zr.Tick(fTimeDelta);
	m_dk.PlaceWASD(m_zpCamera, fTimeDelta);
	if (m_dm.ButtonDownLeft()) {
		bFalling = true;
	}
	if (m_dm.ButtonDownRight()) {
		bFalling = false;
		m_zpS[0].Translate(3.f, 2.5f, -2.5f);
		m_zpS[1].Translate(1.f, 2.5f, -2.5f);
		speed[0] = speed[1] = speed[0] * 0;

	}

	if (bFalling) {
		speed[0] = speed[0] + (gravity * fTimeDelta);
		speed[1] = speed[1] + (gravity * fTimeDelta);
		m_zpS[0].TranslateDelta(speed[0] * fTimeDelta);
		m_zpS[1].TranslateDelta(speed[1] * fTimeDelta);
	}
}

void CGame::Fini()
{
	// Hier die Finalisierung Deiner Vektoria-Objekte einfügen:
}

void CGame::WindowReSize(int iNewWidth, int iNewHeight)
{
	// Windows ReSize wird immer automatisch aufgerufen, wenn die Fenstergröße verändert wurde.
	// Hier kannst Du dann die Auflösung des Viewports neu einstellen:
	m_zf.ReSize(iNewWidth, iNewHeight);
}

