#include "Cannonball.h"

Cannonball::Cannonball()
{
	speed = CHVector();
	sphere.Init(0.5f, NULL);
	this->AddGeo(&sphere);
}

Cannonball::Cannonball(CHVector position, CHVector startForce)
{
	speed = startForce;
	sphere.Init(0.5f, NULL);
	this->AddGeo(&sphere);
	this->Translate(position);
}

Cannonball::~Cannonball()
{
}

void Cannonball::Init(CHVector startForce)
{
	speed = startForce;
}

void Cannonball::HandlePhysics(float ftimeDelta)
{
	speed = speed * (gravity * ftimeDelta);
	this->TranslateDelta(speed * ftimeDelta);
	lifeTime += ftimeDelta;
}

