#pragma once
#include <Vektoria\Placement.h>
#include <Vektoria\GeoSphere.h>
using namespace Vektoria;

class Cannonball : public CPlacement
{
public:
	Cannonball();
	Cannonball(CHVector position, CHVector startForce);
	~Cannonball();
	void Init(CHVector startForce);
	void HandlePhysics(float ftimeDelta);
	
	bool active = false;
	const float gravity = -9.81f;
	float lifeTime = 0;

private:
	CHVector speed;
	CGeoSphere sphere;
};

