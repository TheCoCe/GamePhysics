#pragma once
#include <Vektoria\Placement.h>
#include <VektoriaMath\HVector.h>
#include <list>
#include "Cannonball.h"
using namespace Vektoria;

class Cannon : public CPlacement
{
public:
	Cannon();
	~Cannon();
	void Tick(float fTimeDelta);
	void Shoot();
	float maxProjectileLifetime = 10.f;
private:
	std::list<Cannonball*> activeList;
	std::list<Cannonball*> passiveList;
};

