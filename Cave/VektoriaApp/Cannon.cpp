#include "Cannon.h"

Cannon::Cannon()
{
}

Cannon::~Cannon()
{
	activeList.clear();
	passiveList.clear();
}

void Cannon::Tick(float fTimeDelta)
{
	for (Cannonball* item : activeList) {
		if (item->lifeTime > maxProjectileLifetime) {
			item->active = false;
			activeList.remove(item);
			passiveList.push_back(item);
		}
		else {
			item->HandlePhysics(fTimeDelta);
		}
	}
}

void Cannon::Shoot()
{
	if (passiveList.size() > 0) {
		Cannonball* item = passiveList.front();
		passiveList.pop_front();
		
		item->Translate(this->GetPos());
		item->Init(CHVector(1.f, 1.f, 1.f));
		item->active = true;

		activeList.push_back(item);
	}
	else
	{
		activeList.push_back(new Cannonball(this->GetPos(), CHVector(1.f, 1.f, 1.f)));
	}
}
