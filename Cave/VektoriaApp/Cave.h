#pragma once
#include <Vektoria\Placement.h>
#include <Vektoria\GeoCube.h>
using namespace Vektoria;

class Cave : public CPlacement
{
public:
	Cave(CHVector size);
	Cave(float x, float y, float z, float w = 0.1f);
	~Cave();
private:
	CPlacement m_zpCave[5];
	CGeoCube m_gcCave[5];
};
