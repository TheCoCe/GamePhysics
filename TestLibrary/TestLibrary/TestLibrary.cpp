// TestLibrary.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Vector.h>
#include <iostream>

using namespace engine;

int main()
{
	
	Vector a, b;
	a.X(5.f);
	a.Y(3.f);
	a.Z(4.f);

	b.X(1.f);
	b.Y(1.f);
	b.Z(1.f);

	b += a;

	std::cout << "X: " << b.X() << ", Y: " << b.Y() << ", Z: " << b.Z();

	getchar();

	return 0;
}

